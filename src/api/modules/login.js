import request from "@/utils/request";
export function login(data) {
  return request({
    url: "/api/login",
    method: "post",
    data: data,
  });
}

//获取验证码
export function getCodeImg() {
  return request({
    url: "/api/api/code",
    method: "get",
  });
}
//获取菜单信息、
export function getMenuList(id) {
  return request({
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    url: `/api/menus/${id}/children`,
    methods: "get",
  });
}
//获取所有菜单
export function getMenus(params) {
  return request({
    header: { "Content-Type": " application/x-www-form-urlencoded " },
    url: "/api/api/menus",
    method: "get",
    params: params,
  });
}
//获取当前用户的意见消息
export function getMessage() {
  return request({
    url: "/api/opinions/messages",
    method: "get",
  });
}
