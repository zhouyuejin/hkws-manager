import _axios from "@/plugins/axios";
export function getList(params) {
    return _axios({
        url: "/api/list",
        method: "get",
        params,
    });
}

