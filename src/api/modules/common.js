import request from "../../utils/request";
export function upload(data) {
  return request({
    header: { "Content-type": "multipart/form-data" },
    url: "/api/storages",
    method: "post",
    data: data,
  });
}
