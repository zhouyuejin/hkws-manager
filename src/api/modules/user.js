import _axios from "@/plugins/axios";
export function getList(params) {
  return _axios({
    url: "/api/user",
    method: "get",
    params,
  });
}

export function addUser(data) {
  return _axios({
    url: "/api/user",
    method: "post",
    data,
  });
}

export function updateUser(id, data) {
  return _axios({
    url: `/api/user/${id}`,
    method: "put",
    data,
  });
}

export function delUser(id) {
  return _axios({
    url: `/api/user/${id}`,
    method: "delete",
  });
}
