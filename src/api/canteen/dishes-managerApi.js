import request from "../../utils/request";
//分页查询菜品列表
export function queryDishesList(params) {
  return request({
    url: "/api/food/page",
    method: "get",
    params: params,
  });
}
//添加菜品
export function addFood(data) {
  return request({
    url: "/api/food",
    method: "post",
    data: data,
  });
}
//批量上架
export function upperFood(data) {
  return request({
    url: "/api/food/up",
    method: "put",
    data: data,
  });
}
//批量下架
export function downFood(data) {
  return request({
    url: "/api/food/down",
    method: "put",
    data: data,
  });
}
// 单条查询;
export function getFood(id) {
  return request({
    url: "/api/food/" + id,
    method: "get",
  });
}
//更新
export function updateFood(id, data) {
  return request({
    url: "/api/food/" + id,
    method: "put",
    data: data,
  });
}
//单条删除
export function delFood(id) {
  return request({
    url: "/api/food/" + id,
    method: "delete",
  });
}
//单条下架
export function downFoods(id) {
  return request({
    url: "/api/food/down/" + id,
    method: "put",
  });
}
//单条下架
export function upFoods(id) {
  return request({
    url: "/api/food/up/" + id,
    method: "put",
  });
}
//获取食堂列表
export function getCanteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
//批量删除
export function delFoods(ids) {
  return request({
    url: "/api/food/",
    method: "delete",
    data: ids,
  });
}
//提交菜品审核
export function foodAudit(ids) {
  return request({
    url: "/api/food/submit",
    method: "put",
    data: ids,
  });
}
//获取菜品类型
export function foodType() {
  return request({
    url: "/api/foodtype/queryAll",
    method: "post",
  });
}
