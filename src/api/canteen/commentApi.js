import request from "../../utils/request";

export function getCommentList(params) {
  return request({
    url: "/api/comment/page",
    method: "get",
    params: params,
  });
}
//获取评论详细信息

export function getCommentDetail(id) {
  return request({
    url: "/api/comment/" + id,
    method: "get",
  });
}
//下一条
export function commentNext(id) {
  return request({
    url: "/api/comment/" + id + "/next",
    method: "get",
  });
}
//单条删除
export function delComment(id) {
  return request({
    url: "/api/comment/" + id,
    method: "delete",
  });
}
//多选删除
export function delMultipleComment(data) {
  return request({
    url: "/api/comment?" + data,
    method: "delete",
  });
}
