import request from "@/utils/request";
//获取餐标分页数据
export function orderList(params) {
  return request({
    url: "/api/reserve/orderList",
    method: "get",
    params: params,
  });
}
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
export function canteen() {
  return request({
    url: "/api/reserve/canteenList",
    method: "get",
  });
}
//获取当前时间
export function nowTime(params) {
  return request({
    url: "/api/reserve/timeChoice",
    method: "get",
    params,
  });
}
//获取套餐类型
export function mealType() {
  return request({
    url: "/api/reserve/eatTypeList",
    method: "get",
  });
}
//通过食堂ID获取餐标
export function mealPrice(params) {
  return request({
    url: "/api/reserve/priceCanteenList",
    method: "get",
    params,
  });
}
//获取包房信息
export function roomList(data) {
  return request({
    url: "/api/reserve/privateRoomMessage",
    method: "post",
    data,
  });
}
//包房预定
export function mealRoom(data) {
  return request({
    url: "/api/reserve/orderSave",
    method: "post",
    data,
  });
}
//通过ID查询包房预定信息
export function roomInfo(id) {
  return request({
    url: `/api/reserve/${id}`,
    method: "get",
  });
}

//获取套餐详情
export function mealDetail(params) {
  return request({
    url: `/api/reserve/comboDetail`,
    method: "post",
    params,
  });
}
//撤回预约
export function cancelOrder(id) {
  return request({
    url: `/api/reserve/${id}/user`,
    method: "delete",
  });
}
