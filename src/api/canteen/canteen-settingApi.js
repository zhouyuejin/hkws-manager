import request from "../../utils/request";
//查询所有
export function getCanteenList() {
  return request({
    url: "/api/canteen/all",
    method: "get",
  });
}
//分页查询食堂信息
export function getCanteenPage(params) {
  return request({
    url: "/api/canteen/page",
    method: "get",
    params: params,
  });
}
//添加食堂信息
export function addCanteen(data) {
  return request({
    url: "/api/canteen",
    method: "post",
    data: data,
  });
}
//查询单条食堂信息
export function queryCanteen(id) {
  return request({
    url: "/api/canteen/" + id,
    method: "get",
  });
}
//编辑食堂信息
export function editCanteen(id, data) {
  return request({
    url: "/api/canteen/" + id,
    method: "put",
    data: data,
  });
}
//删除单条食堂信息
export function delCanteen(id) {
  return request({
    url: "/api/canteen/" + id,
    method: "delete",
  });
}
//删除多条食堂信息
export function delMultipleCanteen(data) {
  return request({
    url: "/api/canteen",
    method: "delete",
    data: data,
  });
}
