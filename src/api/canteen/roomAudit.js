/* 包房审核接口*/
import request from "@/utils/request";
//获取餐标分页数据
export function roomList(params) {
  return request({
    url: "/api/managerAudit/auditList",
    method: "get",
    params: params,
  });
}
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
//获取审核详情
export function auditInfo(id) {
  return request({
    url: `/api/managerAudit/auditDetail/${id}`,
    method: "get",
  });
}
//审核驳回
export function audit(data) {
  return request({
    url: `/api/managerAudit/audit`,
    method: "post",
    data,
  });
}
//撤销审核
export function cancelOrder(id) {
  return request({
    url: `/api/managerAudit/${id}/admin`,
    method: "delete",
  });
}
