import request from "@/utils/request";
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
// 分页查询
export function auditPage(params) {
  return request({
    url: "/api/canteen-conf/page",
    method: "get",
    params,
  });
}
// 添加食堂审核人
export function addUser(params) {
  return request({
    url: "/api/canteen-conf",
    method: "post",
    params,
  });
}
// 获取审核人数据
export function userList() {
  return request({
    url: "/api/canteen-conf/allCanteenAudit",
    method: "get",
  });
}
//删除食堂审核人
export function delUser(params) {
  return request({
    url: "/api/canteen-conf",
    method: "delete",
    params,
  });
}
//通过ID获取详情
export function user(id) {
  return request({
    url: `/api/canteen-conf/${id}`,
    method: "get",
    id,
  });
}
//更新审核数据
export function updateUser(id, params) {
  return request({
    url: `/api/canteen-conf/${id}`,
    method: "put",
    params,
  });
}
