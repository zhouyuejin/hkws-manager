import request from "../../utils/request";

//获取处理分页数据
export function queryFeedback(params) {
  return request({
    url: "/api/opinions/page",
    method: "get",
    params: params,
  });
}

//获取意见详情信息
export function getFeedBackDetail(id) {
  return request({
    url: "/api/opinions/" + id,
    method: "get",
  });
}
//获取我的意见处理历史
export function getFeedBackHistory(params) {
  return request({
    url: "/api/opinions/history",
    method: "get",
    params: params,
  });
}
//获取意见处理流程
export function getFeedBackProcess(id) {
  return request({
    url: "/api/opinions/" + id + "/process",
    method: "get",
  });
}
//处理意见
export function handleDetail(id, data) {
  return request({
    url: "/api/opinions/" + id + "/details",
    method: "put",
    data: data,
  });
}
//获取有处理意见权限的用户
export function getHandleOption() {
  return request({
    url: "/api/users",
    method: "get",
  });
}
// 服务回复
export function optionReply(data) {
  return request({
    url: "/api/opinions/reply",
    method: "post",
    params: data,
  });
}
