import request from "../../utils/request";
//菜品审核列表分页查询
export function foodAudit(params) {
  return request({
    url: "/api/canteen/audit/food/page",
    method: "get",
    params: params,
  });
}
// 获取菜谱审核分页列表
export function menuAudit(params) {
  return request({
    url: "/api/canteen/audit/foodMenu/page",
    method: "get",
    params: params,
  });
}

// 菜谱审核
export function menuSubmitAudit(id, data) {
  return request({
    url: `/api/canteen/audit/foodMenu/${id}`,
    method: "POST",
    data: data,
  });
}

// 获取菜品详情
export function foodAuditDetail(id) {
  return request({
    url: `/api/food/${id}`,
    method: "get",
  });
}
// 获取菜谱详情
export function menuAuditDetail(id) {
  return request({
    url: "/api/food-week/queryByid/" + id,
    method: "get",
  });
}
// 菜品审核
export function foodSubmitAudit(id, data) {
  return request({
    url: `/api/canteen/audit/food/${id}`,
    method: "POST",
    data: data,
  });
}
// 菜品批量提交审核
export function foodMultipleSubmit(data) {
  return request({
    url: "/api/canteen/audit/food",
    method: "post",
    params: data,
  });
}
// 菜谱批量提交审核
export function menuMultipleSubmit(data) {
  return request({
    url: "/api/canteen/audit/foodMenu",
    method: "post",
    params: data,
  });
}
