import request from "@/utils/request";
//获取包房分页数据
export function mealList(params) {
  return request({
    url: "/api/combo/page",
    method: "get",
    params: params,
  });
}
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
//根据食堂ID获取餐标信息
export function mealOption(params) {
  return request({
    url: "/api/combo/priceByCanteen",
    method: "get",
    params,
  });
}
//添加套餐信息
export function addMeal(data) {
  return request({
    url: "/api/combo",
    method: "post",
    data,
  });
}
//删除套餐信息
export function delMeal(params) {
  return request({
    url: "/api/combo",
    method: "delete",
    params,
  });
}
//通过ID获取套餐详情
export function mealInfo(id) {
  return request({
    url: `/api/combo/${id}`,
    method: "get",
  });
}
//更新套餐信息
export function updateMeal(id, data) {
  return request({
    url: `/api/combo/${id}`,
    method: "put",
    data,
  });
}
