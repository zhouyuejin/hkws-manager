import request from "@/utils/request";
//获取包房分页数据
export function roomList(params) {
  return request({
    url: "/api/chartered-room/page",
    method: "get",
    params: params,
  });
}
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}

//添加包房数据
export function roomData(data) {
  return request({
    url: "/api/chartered-room",
    method: "post",
    data,
  });
}
//删除包房数据
export function delRoom(params) {
  return request({
    url: "/api/chartered-room",
    method: "delete",
    params,
  });
}
//根据ID查询包房信息
export function roomInfo(id) {
  return request({
    url: `/api/chartered-room/${id}`,
    method: "get",
  });
}
//更新包房信息
export function updateRoom(id, data) {
  return request({
    url: `/api/chartered-room/${id}`,
    method: "put",
    data,
  });
}
