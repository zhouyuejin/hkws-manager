import request from "@/utils/request";
//获取餐标分页数据
export function mealList(params) {
  return request({
    url: "/api/price/page",
    method: "get",
    params: params,
  });
}
//获取食堂数据
export function canteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
//添加餐标
export function addPrice(data) {
  return request({
    url: "/api/price",
    method: "post",
    data,
  });
}
//删除餐标
export function delPrice(params) {
  return request({
    url: "/api/price",
    method: "delete",
    params,
  });
}
//通过ID查询餐标信息
export function priceInfo(id) {
  return request({
    url: `/api/price/${id}`,
    method: "get",
  });
}
//通过ID查询餐标信息
export function updatePrice(id, data) {
  return request({
    url: `/api/price/${id}`,
    method: "put",
    data,
  });
}
//通过餐标查询默认套餐
export function defalutMeal(id) {
  return request({
    url: `/api/price/getCombo/${id}`,
    method: "get",
  });
}
