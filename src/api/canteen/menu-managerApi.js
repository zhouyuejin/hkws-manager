import request from "../../utils/request";
//分页查询
export function queryMenuList(params) {
  return request({
    url: "/api/food-week/page",
    method: "get",
    params: params,
  });
}
//获取食堂列表
export function getCanteenList() {
  return request({
    url: "/api/canteen/list",
    method: "get",
  });
}
//获取菜谱信息
export function getFoodMenu() {
  return request({
    url: "/api/app/food-menu/detail",
    method: "get",
  });
}
//根据食堂的id获取菜品列表
export function getCanteenFoodList(id) {
  return request({
    url: "/api/app/food/canteen/" + id,
    method: "get",
  });
}
//菜谱数据添加
export function addFoodMenu(data) {
  return request({
    url: "/api/food-menu",
    method: "post",
    data: data,
  });
}

//根据菜谱id单条查询
export function getFoodMenuList(id) {
  return request({
    url: "/api/food-week/queryByid/" + id,
    method: "get",
  });
}

//菜谱导入
export function foodMenImport(data) {
  return request({
    url: "/api/food-week/import",
    method: "post",
    data: data,
    headers: { "Content-Type": "multipart/form-data" },
  });
}
//菜谱导出
export function foodMenuExport(data) {
  return request({
    url: "/api/food-week/export",
    method: "post",
    params: data,
    responseType: "blob",
  });
}
//根据菜谱id单条查询
export function delFoodMenu(id) {
  return request({
    url: "/api/food-menu/" + id,
    method: "delete",
  });
}
//全选删除
export function delMultiple(ids) {
  return request({
    url: "/api/food-menu/",
    method: "delete",
    data: ids,
  });
}
//更新数据
export function updateFoodMenu(id, data) {
  return request({
    url: "/api/food-menu/" + id,
    method: "put",
    data: data,
  });
}
//菜品检索
export function queryFood(id, params) {
  return request({
    url: "/api/food/" + id + "/name",
    method: "get",
    params: params,
  });
}
//获取碗碗菜价格
export function getBlowsPrice() {
  return request({
    url: "/api/bowl/price",
    method: "get",
  });
}
// 菜品批量提交
export function menuAudit(ids) {
  return request({
    url: "/api/food-menu/submit",
    method: "put",
    data: ids,
  });
}

//周菜谱单条删除菜谱
export function delMenu(id) {
  return request({
    url: `/api/food-week/delete/${id}`,
    method: "delete",
  });
}
//周菜谱多条删除

export function delMutMenu(data) {
  return request({
    url: "/api/food-week/deletes/",
    method: "delete",
    data,
  });
}
// 周菜谱单条查询
export function foodWeek(id) {
  return request({
    url: `/api//food-week/queryByid/${id}`,
    method: "get",
  });
}
// 周菜谱提交
export function foodWeekSubmit(data) {
  return request({
    url: "/api/food-week/submit",
    method: "put",
    data,
  });
}
// 周菜谱批量下架
export function foodWeekDown(data) {
  return request({
    url: "/api/food-week/takedown",
    method: "put",
    data,
  });
}
