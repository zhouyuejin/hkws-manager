import request from "../utils/request";
//修改用户密码
export function updatePassword(data) {
  return request({
    url: "/api/users/password",
    method: "put",
    data: data,
  });
}
