import request from "../../utils/request";

//分页查询
export function queryLog(params) {
  return request({
    url: "/api/logs",
    method: "get",
    params: params,
  });
}
