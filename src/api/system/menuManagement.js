import request from "../../utils/request";

//根据子节点获取表单数据

export function getMenuList() {
  return request({
    url: "/api/menus/0/children",
    method: "get",
  });
}
//添加节点接口
export function addNodeList(data) {
  return request({
    url: "/api/menus",
    method: "post",
    data: data,
  });
}
//删除节点接口

export function delNodeList(id) {
  return request({
    url: "/api/menus/" + id,
    method: "delete",
  });
}
//编辑节点接口
export function editNodeList(data, id) {
  return request({
    url: "/api/menus/" + id,
    method: "put",
    data: data,
  });
}
