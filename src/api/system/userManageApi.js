//账户管理api
import request from "../../utils/request";
//分页查询
export function getUserList(params) {
  return request({
    url: "/api/users/page",
    method: "get",
    params: params,
  });
}
//获取用户基本信息
export function getUserInfoList(id) {
  return request({
    url: "/api/users/" + id,
    method: "get",
  });
}
//修改用户信息
export function editUserInfo(id, data) {
  return request({
    url: "/api/users/" + id + "/info",
    method: "put",
    data: data,
  });
}

//新增用户
export function addUserInfo(data) {
  return request({
    url: "/api/users",
    method: "post",
    data: data,
  });
}

//删除用户
export function delUserInfo(data) {
  return request({
    url: "/api/users",
    method: "delete",
    data: data,
  });
}

//启用禁用
export function enabledUserInfo(data) {
  return request({
    url: "/api/users/enabled",
    method: "put",
    data: data,
  });
}
//根据用户查询获取所有食堂

export function getCanteenList() {
  return request({
    url: "/api/canteen/all",
    method: "get",
  });
}
//修改权限
export function editUserAuthor(id, data) {
  return request({
    url: "/api/users/" + id + "/auth",
    method: "put",
    data: data,
  });
}
//重置密码
export function resetPassword(id) {
  return request({
    url: "/api/users/" + id + "/password/reset",
    method: "put",
  });
}
