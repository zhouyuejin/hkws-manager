import request from "../../utils/request";

//获取所有角色
export function getRoleList() {
  return request({
    url: "/api/roles",
    method: "get",
  });
}

//新增角色
export function addRoleList(data) {
  return request({
    url: "/api/roles",
    method: "post",
    data: data,
  });
}

//获取用户详细信息
export function getRoleInfoList(id) {
  return request({
    url: "/api/roles/" + id,
    method: "get",
  });
}

//编辑角色
export function editRoleInfoList(id, data) {
  return request({
    url: "/api/roles/" + id,
    method: "put",
    data: data,
  });
}
//删除角色
export function delRoleInfoList(id) {
  return request({
    url: "/api/roles/" + id,
    method: "delete",
  });
}
