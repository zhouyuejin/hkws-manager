import request from "../../utils/request";

//根据父级ID返回所有子节点
export function getDepartmentsList() {
  return request({
    url: "/api/departments/" + 0 + "/children",
    method: "get",
  });
}

//获取部门负责人
export function getDepartmentsMonitors(id, params) {
  return request({
    url: "/api/departments/" + id + "/monitors",
    method: "get",
    params: params,
  });
}

//新增部门信息
export function addDepartmentList(data) {
  return request({
    url: "/api/departments",
    method: "post",
    data: data,
  });
}

//获取职位列表
export function getPositionsList(params) {
  return request({
    url: "/api/positions/page",
    method: "get",
    params: params,
  });
}

//获取部门职能

export function getCompetencesList(params) {
  return request({
    url: "/api/competences/page",
    method: "get",
    params: params,
  });
}

//获取区域列表

export function getAreasList(id) {
  return request({
    url: "/api/areas/" + id + "/children",
    method: "get",
  });
}

//获取部门负责人
export function getMonitorData() {
  return request({
    url: "/api/users/page",
    method: "get",
  });
}

//添加部门负责任人
export function addMonitors(id, data) {
  return request({
    url: "/api/departments/" + id + "/monitors",
    method: "post",
    data: data,
  });
}

//修改节点数据

export function editDepartment(id, data) {
  return request({
    url: "/api/departments/" + id,
    method: "put",
    data: data,
  });
}

//删除节点数据
export function delDepartment(id) {
  return request({
    url: "/api/departments/" + id,
    method: "delete",
  });
}
//修改部门负责人
export function editMonitors(monitorId, data) {
  return request({
    url: "/api/departments/monitors/" + monitorId,
    method: "put",
    data: data,
  });
}
//删除部门负责人
export function delMonitors(monitorId) {
  return request({
    url: "/api/departments/monitors/" + monitorId,
    method: "delete",
  });
}
