import request from "../../utils/request";

//获取部门职能
export function getCompetences(params) {
  return request({
    url: "/api/competences/page",
    method: "get",
    params: params,
  });
}

//获取员工等级分页数据
export function getGrades(params) {
  return request({
    url: "/api/grades/page",
    method: "get",
    params: params,
  });
}

// 获取员工职位
export function getPositions(params) {
  return request({
    url: "/api/positions/page",
    method: "get",
    params: params,
  });
}

//获取离职原因
export function getResignations(params) {
  return request({
    url: "/api/resignations/page",
    method: "get",
    params: params,
  });
}
//添加部门职能
export function addCompetences(data) {
  return request({
    url: "/api/competences",
    method: "post",
    data: data,
  });
}
//添加员工职位
export function addPositions(data) {
  return request({
    url: "/api/positions",
    method: "post",
    data: data,
  });
}
//添加员工级别
export function addGrades(data) {
  return request({
    url: "/api/grades",
    method: "post",
    data: data,
  });
}
//添加离职原因
export function addResignations(data) {
  return request({
    url: "/api/resignations",
    method: "post",
    data: data,
  });
}
//修改部门职能
export function editCompetences(id, data) {
  return request({
    url: "/api/competences/" + id,
    method: "put",
    data: data,
  });
}
//删除部门职能
export function delCompetences(id) {
  return request({
    url: "/api/competences/" + id,
    method: "delete",
  });
}
//删除部门职能
export function delMultipleCompetences(ids) {
  return request({
    url: "/api/competences",
    method: "delete",
    data: ids,
  });
}
//修改职位
export function editPositions(id, data) {
  return request({
    url: "/api/positions/" + id,
    method: "put",
    data: data,
  });
}
//删除职位
export function delPositions(id) {
  return request({
    url: "/api/positions/" + id,
    method: "delete",
  });
}
//删除部门职能
export function delMultiplePositions(ids) {
  return request({
    url: "/api/positions",
    method: "delete",
    data: ids,
  });
}
//修改级别
export function editGrades(id, data) {
  return request({
    url: "/api/grades/" + id,
    method: "put",
    data: data,
  });
}
//删除级别
export function delGrades(id) {
  return request({
    url: "/api/grades/" + id,
    method: "delete",
  });
}

//删除部门职能
export function delMultipleGrades(ids) {
  return request({
    url: "/api/grades",
    method: "delete",
    data: ids,
  });
}
//修改离职原因
export function editResignations(id, data) {
  return request({
    url: "/api/resignations/" + id,
    method: "put",
    data: data,
  });
}
//删除离职原因
export function delResignations(id) {
  return request({
    url: "/api/resignations/" + id,
    method: "delete",
  });
}
//删除离职原因
export function delMultipleResignations(ids) {
  return request({
    url: "/api/resignations",
    method: "delete",
    data: ids,
  });
}
