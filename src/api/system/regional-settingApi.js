import request from "../../utils/request";

//根据父级ID返回所有子节点
export function getAreas(id) {
  return request({
    url: "/api/areas/" + id + "/children",
  });
}

//新增父级信息
export function addParentList(data) {
  return request({
    url: "/api/areas",
    method: "post",
    data: data,
  });
}

//编辑
export function editParentList(id, data) {
  return request({
    url: "/api/areas/" + id,
    method: "put",
    data: data,
  });
}
//删除
export function delParentList(id) {
  return request({
    url: "/api/areas/" + id,
    method: "delete",
  });
}
//获取区域信息
export function queryAreas(params) {
  return request({
    url: "/api/areas",
    method: "get",
    params: params,
  });
}
