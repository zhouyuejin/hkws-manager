import request from "@/utils/request";
//获取曝光台分页数据
export function exposeList(params) {
  return request({
    url: "/api/exposureStage/list",
    method: "get",
    params: params,
  });
}
//获取标签数据
export function tagsList() {
  return request({
    url: "/api/tag/all",
    method: "get",
  });
}
//添加曝光台
export function addExpose(data) {
  return request({
    url: "/api/exposureStage/add",
    method: "post",
    data,
  });
}
//删除曝光台数据
export function delExpose(data) {
  return request({
    url: "/api/exposureStage/delete",
    method: "delete",
    data,
  });
}
//通过ID获取曝光台详情
export function exposeInfo(id) {
  return request({
    url: `/api/exposureStage/${id}`,
    method: "get",
  });
}
//通过ID修改曝光台信息
export function updateExpose(data) {
  return request({
    url: `/api/exposureStage/update`,
    method: "post",
    data,
  });
}
//单条删除曝光台数据
export function exposureStage(id) {
  return request({
    url: `/api/exposureStage/${id}`,
    method: "delete",
  });
}
//获取分页标签数据
export function tagPage(params) {
  return request({
    url: `/api/tag/page`,
    method: "get",
    params,
  });
}
//添加标签数据
export function addTag(params) {
  return request({
    url: `/api/tag/add`,
    method: "post",
    params,
  });
}
//多选删除标签
export function delTag(data) {
  return request({
    url: `/api/tag`,
    method: "delete",
    data,
  });
}
//通过ID获取标签信息
export function tagInfo(id) {
  return request({
    url: `/api/tag/${id}`,
    method: "get",
  });
}
//更新标签信息
export function updateTag(data) {
  return request({
    url: `/api/tag/update`,
    method: "put",
    data,
  });
}
