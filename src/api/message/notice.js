import request from "@/utils/request";
//获取通知公告分页数据
export function notice(params) {
  return request({
    url: "/api/announcement/editList",
    method: "get",
    params: params,
  });
}
//获取单位数据
export function department(id) {
  return request({
    url: `/api/departments/${id}/children`,
    method: "get",
  });
}
//添加单位数据
export function addNotices(data) {
  return request({
    url: `/api/announcement/add`,
    method: "post",
    data,
  });
}
//批量发布
export function publish(data) {
  return request({
    url: `/api/announcement/publish`,
    method: "post",
    data,
  });
}
//批量取消发布
export function unpublish(data) {
  return request({
    url: `/api/announcement/unpublish`,
    method: "post",
    data,
  });
}
//批量删除
export function delpublish(data) {
  return request({
    url: `/api/announcement/delete`,
    method: "delete",
    data,
  });
}
//获取详情信息
export function detail(id) {
  return request({
    url: `/api/announcement/edit/${id}`,
    method: "get",
  });
}
//修改
export function updateNotice(data) {
  return request({
    url: `/api/announcement/update`,
    method: "post",
    data,
  });
}
