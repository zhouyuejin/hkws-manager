export default function commonPageRepository(pageSize, pageNum, get) {
  const pageSizeChange = (currentPageSize) => {
    pageSize.value = currentPageSize;
    get();
  };

  const pageCurrentChange = (currentPageNum) => {
    pageNum.value = currentPageNum;
    get();
  };

  return {
    pageSizeChange,
    pageCurrentChange,
  };
}
