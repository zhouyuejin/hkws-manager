import { onMounted, reactive, ref } from "vue";
import { ElMessageBox, ElMessage } from "element-plus";
import {
  addParentList,
  delParentList,
  editParentList,
  getAreas,
  queryAreas,
} from "../../api/system/regional-settingApi";
export default function RegionalSettings() {
  //表格数据
  const tableData = ref([]);
  const addForm = reactive({});
  const editForm = reactive({});
  const addFormRef = ref(null);
  //新增对话框显示与隐藏
  const addDialogVisible = ref(false);
  //编辑对话框显示与隐藏
  const editDialogVisible = ref(false);
  //用户ID
  const userId = ref("");
  //父级id
  const parentId = ref("");
  //树形筛选
  const filterText = ref("");
  const queryKeyCode = ref("");
  //添加数据方法
  function addParent(row) {
    userId.value = row.id;
    parentId.value = row.parentId;
    addDialogVisible.value = true;
  }

  //提交父级新增信息
  function submitParent() {
    addParentList({
      name: addForm.name,
      parentId: parentId.value === undefined ? 0 : userId.value,
      sort: tableData.value.length + 1,
      description: addForm.desc,
    }).then(() => {
      getAreaData();
      ElMessage({
        message: "添加成功",
        type: "success",
      });
      addFormRef.value.resetFields();
      addDialogVisible.value = false;
    });
  }

  //编辑操作
  function edit(row) {
    console.log(row);
    userId.value = row.id;
    editDialogVisible.value = true;
    editForm.parentName = row.fullName;
    editForm.name = row.name;
    editForm.description = row.description;
  }
  //提交编辑信息
  function submitEdit() {
    editParentList(userId.value, {
      description: editForm.description,
      name: editForm.name,
      sort: tableData.value.length + 1,
    }).then(() => {
      ElMessage({
        message: "编辑成功",
        type: "success",
      });
      editDialogVisible.value = false;
      getAreaData();
    });
  }
  //根据父级Id获取数据
  function getAreaData() {
    getAreas(0).then((res) => {
      tableData.value = res.data;
    });
  }

  const parentOption = ref([]);
  const tableRef = ref(null);
  //删除操作
  function del(id) {
    ElMessageBox.confirm("您确定要删除这条数据么？", "提示", {
      confirmButtonText: "确定",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        delParentList(id).then(() => {
          ElMessage({
            message: "删除成功",
            type: "success",
          });
          getAreaData();
        });
      })
      .catch(() => {
        ElMessage({
          type: "info",
          message: "取消操作",
        });
      });
  }
  onMounted(() => {
    getAreaData();
  });
  //重置
  function reset() {
    filterText.value = "";
  }
  //搜索
  function search() {
    queryAreas({
      keyword: filterText.value,
    }).then((res) => {
      tableData.value = res.data;
    });
  }
  return {
    search,
    reset,
    addFormRef,
    addForm,
    parentOption,
    editForm,
    editDialogVisible,
    del,
    edit,
    tableData,
    tableRef,
    addDialogVisible,
    addParent,
    submitParent,
    filterText,
    queryKeyCode,
    submitEdit,
  };
}
