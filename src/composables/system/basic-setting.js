import { onMounted, ref } from "vue";

export default function basicSetting() {
  //当前选中下标
  const current = ref(0);

  function change(index) {
    if (current.value != index) {
      current.value = index;
    }
  }

  onMounted(() => {
    change(0);
  });
  return {
    current,
    change,
  };
}
