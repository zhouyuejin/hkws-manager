import { onMounted, ref } from "vue";
import { useRouter, useRoute } from "vue-router";
import { getRoleList } from "@/api/system/roleManageApi";
import { getUserInfoList } from "@/api/system/userManageApi";
import { editUserAuthor, getCanteenList } from "../../api/system/userManageApi";
import { ElMessage } from "element-plus";
import Cookies from "js-cookie";
export default function userAuthorize() {
  const router = useRouter();
  const route = useRoute();
  const treeRef = ref(null);
  //用户名
  const userName = ref("");
  //手机号
  const phone = ref("");
  //部门
  const bumen = ref("");
  //用户Id
  const userId = ref("");
  //角色拥有权限
  const hasData = ref([]);
  //用户拥有
  const hasUserData = ref([]);
  //对话框显示与隐藏
  const addDialogVisible = ref(false);
  //用户拥有系统权限
  const userHasSystemData = ref([]);
  // 选中	角色id集合
  const userIds = ref([]);
  const roleData = ref([]);
  const canteenIds = ref([]);
  const canteenRef = ref(null);
  const roleIds = ref([]);
  const canteenData = ref([]);
  //返回上一页面
  function back() {
    router.push("/system/user-management");
  }
  //提交修改权限
  function submitAuthorize() {
    hasUserData.value.map((item) => {
      roleIds.value.push(item.id);
    });
    userHasSystemData.value.map((canteen) => {
      canteenData.value.push(canteen.id);
    });
    editUserAuthor(userId.value, {
      roleIds: roleIds.value,
      canteenIds: canteenData.value,
    })
      .then((res) => {
        if (res.code === 200) {
          ElMessage({
            message: "修改权限成功",
            type: "success",
          });
          getRoleData();
        } else if (res.code === 401) {
          Cookies.remove("token");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  //单选框操作
  function change(node) {
    hasData.value.push(node);
  }
  //获取所有角色
  function getRoleData() {
    getRoleList().then((res) => {
      roleData.value = res.data;
      //获取用户基本信息
      getUserInfoList(userId.value).then((res) => {
        userName.value = res.data.nickname;
        phone.value = res.data.mobile;
        bumen.value = res.data.departments;
        userIds.value = res.data.roleId;
        canteenIds.value = res.data.canteenIds;
        //设置数据选中
        treeRef.value.setCheckedKeys(userIds.value, true);
        canteenRef.value.setCheckedKeys(canteenIds.value, true);
      });
    });
  }
  const defaultProps = { children: "children", label: "name" };
  //获取所有食堂信息
  function getCateenData() {
    getCanteenList().then((res) => {
      systemData.value = res.data;
    });
  }
  //系统树
  const systemData = ref([]);
  //复选框操作
  function userChecked(node, checked) {
    //判断选中ID是否存在数组中
    let index = hasUserData.value.find((checkd) => checkd.id === node.id);
    if (index === undefined && checked === true) {
      hasUserData.value.push(node);
    } else {
      //删除数组中对应Id的数据
      hasUserData.value.forEach((item, index) => {
        if (item.id === node.id) {
          hasUserData.value.splice(index, 1);
        }
      });
    }
  }
  //数据复选框操作
  function systemChecked(node, checked) {
    const systemIndex = userHasSystemData.value.find(
      (checkd) => checkd.id === node.id
    );
    if (systemIndex === undefined && checked === true) {
      userHasSystemData.value.push(node);
    } else {
      userHasSystemData.value.forEach((item, index) => {
        if (item.id === node.id) {
          userHasSystemData.value.splice(index, 1);
        }
      });
    }
  }
  onMounted(() => {
    userId.value = route.query.userId;
    getRoleData();
    getCateenData();
  });
  return {
    back,
    defaultProps,
    addDialogVisible,
    userName,
    userChecked,
    systemData,
    phone,
    bumen,
    treeRef,
    userHasSystemData,
    canteenRef,
    change,
    submitAuthorize,
    hasUserData,
    systemChecked,
    hasData,
    roleData,
  };
}
