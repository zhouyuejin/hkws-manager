import { onMounted, reactive, ref } from "vue";
import { ElMessage, ElMessageBox } from "element-plus";
import {
  addRoleList,
  delRoleInfoList,
  editRoleInfoList,
  getRoleInfoList,
  getRoleList,
} from "../../api/system/roleManageApi";
import { getMenuList } from "../../api/system/menuManagement";

export default function roleManagement() {
  //获取所有用户
  function getRoleData() {
    getRoleList().then((res) => {
      roleList.value = res.data;
    });
  }
  const dialogTitle = ref("");
  const ruleForm = ref(null);
  const treeRef = ref(null);
  //  编辑对话框显示与隐藏
  const dialogVisible = ref(false);
  //角色数据
  const roleList = ref([]);
  //表单验证规则
  const editRule = reactive({
    name: [
      {
        required: true,
        message: "请输入角色名称",
        trigger: "blur",
      },
    ],
    remark: [
      {
        required: true,
        message: "请输入职责说明",
        trigger: "blur",
      },
    ],
  });
  //权限树
  const rightData = ref([]);
  //树节点选中操作

  //用户ID
  const roleId = ref(Number);
  const defaultProps = { children: "children", label: "name" };
  //操作树
  const optionTree = ref([]);
  //编辑表单
  const editForm = reactive({});
  //提交操作
  function submit() {
    if (roleId.value) {
      //获取当前选中节点的key
      const checkId = treeRef.value
        .getCheckedKeys()
        .concat(treeRef.value.getHalfCheckedKeys());
      console.log(checkId);
      editRoleInfoList(roleId.value, {
        menuIds: checkId,
        name: editForm.name,
        remark: editForm.remark,
      }).then(() => {
        ElMessage({
          message: "操作成功",
          type: "success",
        });
        dialogVisible.value = false;
        //重置表单数据
        ruleForm.value.resetFields();
        getRoleData();
      });
    } else {
      const checkId = treeRef.value
        .getCheckedKeys()
        .concat(treeRef.value.getHalfCheckedKeys());
      addRoleList({
        menuIds: checkId,
        name: editForm.name,
        remark: editForm.remark,
      }).then(() => {
        dialogVisible.value = false;
        ElMessage({
          message: "操作成功",
          type: "success",
        });
        //重置表单数据
        ruleForm.value.resetFields();
        dialogVisible.value = false;
        getRoleData();
      });
    }
  }
  //删除用户操作
  function del(id) {
    ElMessageBox.confirm("您确定要删除当前角色么?", "警告", {
      confirmButtonText: "确定",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        delRoleInfoList(id)
          .then(() => {
            roleList.value.splice(id, 1);
            ElMessage({
              type: "success",
              message: "删除成功",
            });
            getRoleData();
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch(() => {
        ElMessage({
          type: "info",
          message: "取消操作",
        });
      });
  }

  //新增编辑用户操作
  function edit(item) {
    dialogVisible.value = true;
    roleId.value = item.id;
    if (roleId.value) {
      dialogTitle.value = "编辑";
      getRoleInfoList(roleId.value).then((res) => {
        editForm.name = res.data.name;
        editForm.remark = res.data.remark;
        treeRef.value.setCheckedKeys(res.data.menuIds, false);
        //设置选中节点
        getMenuData();
      });
    } else {
      dialogTitle.value = "新增";
      getMenuData();
    }
  }
  //取消关闭对话框
  function cancel() {
    treeRef.value.setCheckedKeys([]);
    ruleForm.value.resetFields();
    dialogVisible.value = false;
  }
  // 关闭对话框时的回调
  function handleClose() {
    treeRef.value.setCheckedKeys([]);
    ruleForm.value.resetFields();
    dialogVisible.value = false;
  }
  //获取权限菜单数据
  function getMenuData() {
    getMenuList().then((res) => {
      rightData.value = res.data;
    });
  }
  onMounted(() => {
    getRoleData();
  });
  return {
    dialogTitle,
    submit,
    cancel,
    ruleForm,
    // changeChecked,
    treeRef,
    optionTree,
    roleList,
    editRule,
    rightData,
    editForm,
    dialogVisible,
    del,
    edit,
    defaultProps,
    handleClose,
  };
}
