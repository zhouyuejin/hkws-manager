import { onMounted, ref } from "vue";
import {
  getFeedBackDetail,
  getFeedBackHistory,
  getFeedBackProcess,
  optionReply,
} from "../../api/canteen/feedbackApi";
import { useRoute } from "vue-router";
import { ElMessage } from "element-plus";

export default function feedbackDetail() {
  const detailForm = ref({});
  const timeline = ref([]);
  const tableData = ref([]);
  // 用户ID
  const userId = ref("");
  const route = useRoute();
  //获取详情信息
  function getFeedbackData() {
    getFeedBackDetail(userId.value).then((res) => {
      detailForm.value = res.data;
    });
  }
  const totalNum = ref(Number);
  const pageSize = ref(5);
  const pageNum = ref(1);
  //获取我的意见详情历史记录
  function getMyHistory() {
    getFeedBackHistory({
      pageNumber: pageNum.value,
      pageSize: pageSize.value,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
    });
  }
  //获取处理意见流程
  function getProcessData() {
    getFeedBackProcess(userId.value).then((res) => {
      timeline.value = res.data;
      timeline.value.map((item) => {
        item.color = "#0bbd87";
        item.icon = "el-icon-success";
      });
      //查找最后一个元素
      timeline.value.slice(-1).map((item) => {
        item.color = "#0bbd87";
        item.icon = "";
      });
    });
  }
  //获取我处理过的历史意见
  function look(row) {
    getFeedBackProcess(row.opinionId).then((res) => {
      timeline.value = res.data;
      timeline.value.map((item) => {
        item.color = "#0bbd87";
        item.icon = "el-icon-success";
      });
      //查找最后一个元素
      timeline.value.slice(-1).map((item) => {
        item.color = "#0bbd87";
        item.icon = "";
      });
    });
  }
  // 回复ID
  const optionId = ref("");
  // 回复对话框显示与隐藏
  const replyDialogVisible = ref(false);
  // 回复内容
  const replyContent = ref("");
  // 回复按钮
  function reply(id) {
    replyDialogVisible.value = true;
    optionId.value = id;
    replyContent.value = "";
  }
  // 提交回复
  function submitReply() {
    if (optionId.value) {
      optionReply({
        id: optionId.value,
        reply: replyContent.value,
      })
        .then(() => {
          ElMessage.success("回复成功");
          replyDialogVisible.value = false;
          replyContent.value = "";
          optionId.value = "";
        })
        .catch(() => {
          replyDialogVisible.value = false;
          replyContent.value = "";
          optionId.value = "";
        });
    } else {
      optionReply({
        id: userId.value,
        reply: replyContent.value,
      })
        .then(() => {
          ElMessage.success("回复成功");
          replyDialogVisible.value = false;
          replyContent.value = "";
        })
        .catch(() => {
          replyDialogVisible.value = false;
          replyContent.value = "";
        });
    }
  }
  // 右侧回复按钮
  function RightOptionReply() {
    replyDialogVisible.value = true;
  }
  onMounted(() => {
    userId.value = route.query.id;
    getFeedbackData();
    getMyHistory();
    getProcessData();
  });
  return {
    detailForm,
    tableData,
    timeline,
    look,
    totalNum,
    pageSize,
    pageNum,
    getMyHistory,
    replyDialogVisible,
    replyContent,
    submitReply,
    reply,
    RightOptionReply,
  };
}
