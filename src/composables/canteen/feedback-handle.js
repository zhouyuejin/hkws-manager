import { onMounted, ref } from "vue";
import { useRoute, useRouter } from "vue-router";
import {
  getFeedBackDetail,
  getFeedBackProcess,
  getHandleOption,
  handleDetail,
} from "../../api/canteen/feedbackApi";
import { ElMessage } from "element-plus";

export default function feedbackHandle() {
  const handleForm = ref({});
  const rejectDialogVisible = ref(false);
  const finishDialogVisible = ref(false);
  const outDialogVisible = ref(false);
  const outForm = ref({});

  function finish() {
    finishDialogVisible.value = true;
  }

  const timeline = ref([]);

  //驳回
  function reject() {
    rejectDialogVisible.value = true;
  }
  //处理权限用户列表
  const handleOptionList = ref([]);
  //转出
  function out() {
    outDialogVisible.value = true;
    //  获取有权限处理的客户
    getHandleOption().then((res) => {
      handleOptionList.value = res.data;
    });
  }
  const followHandler = ref("");
  function changeChecke() {
    optionForm.value.map((item) => {
      followHandler.value = item.id;
    });
  }
  //提交转处
  function submitOut() {
    handleDetail(userId.value, {
      followHandler: followHandler.value,
      handleMessage: outForm.value.name,
      handleType: 0,
    })
      .then(() => {
        ElMessage({
          message: "转处成功",
          type: "success",
        });
        outDialogVisible.value = false;
        getHandleDetail();
        getHandleProcess();
      })
      .catch(() => {
        outDialogVisible.value = false;
        getHandleDetail();
      });
  }
  const optionForm = ref([]);
  //驳回原因
  const rejectContent = ref("");
  //驳回方法
  function submitReject() {
    handleDetail(userId.value, {
      handleMessage: rejectContent.value,
      handleType: 1,
    })
      .then(() => {
        rejectDialogVisible.value = false;
        ElMessage.success("驳回成功");
        getHandleDetail();
        getHandleProcess();
        cancel();
        rejectContent.value = "";
      })
      .catch(() => {
        rejectContent.value = "";
        rejectDialogVisible.value = false;
      });
  }
  //用户ID
  const userId = ref("");
  const route = useRoute();
  //获取详情
  function getHandleDetail() {
    getFeedBackDetail(userId.value).then((res) => {
      handleForm.value = res.data;
    });
  }
  //获取意见处理流程
  function getHandleProcess() {
    getFeedBackProcess(userId.value).then((res) => {
      timeline.value = res.data;
      console.log(timeline.value);
      timeline.value.map((item) => {
        (item.color = "#00B913"), (item.icon = "el-icon-success");
      });
      //查找最后一个元素
      timeline.value.slice(-1).map((item) => {
        item.color = "#0bbd87";
        item.icon = "";
      });
    });
  }
  //办结原因
  const finishContent = ref("");
  //提交办结
  function submitFinsh() {
    handleDetail(userId.value, {
      handleMessage: finishContent.value,
      handleType: 2,
    })
      .then(() => {
        ElMessage({
          message: "办结成功",
          type: "success",
        });
        getHandleDetail();
        getHandleProcess();
        finishDialogVisible.value = false;
        cancel();
      })
      .catch(() => {
        finishDialogVisible.value = false;
      });
  }
  const router = useRouter();
  //取消
  function cancel() {
    router.back(-1);
  }
  //处理人id
  //获取当前用户id
  // const user = JSON.parse(sessionStorage.getItem("userInfo")).id;
  const currentHandlerId = ref("");
  onMounted(() => {
    userId.value = route.query.id;
    currentHandlerId.value = route.query.currentHandlerId;
    getHandleDetail();
    getHandleProcess();
  });
  return {
    // user,
    handleForm,
    outForm,
    rejectDialogVisible,
    finish,
    finishDialogVisible,
    reject,
    out,
    outDialogVisible,
    timeline,
    finishContent,
    submitFinsh,
    rejectContent,
    submitReject,
    handleOptionList,
    optionForm,
    changeChecke,
    submitOut,
    cancel,
    currentHandlerId,
  };
}
