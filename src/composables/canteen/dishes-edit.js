import { reactive } from "vue";

export default function dishesEdit() {
  const form = reactive({});
  return {
    form,
  };
}
