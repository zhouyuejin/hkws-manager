import { ref, onMounted, reactive, watch } from "vue";
import router from "@/router";
import { queryFeedback } from "../../api/canteen/feedbackApi";
import { useRoute } from "vue-router";
export default function feedback() {
  const tableData = ref([]);
  // 分页数据
  const pageSize = ref(50);
  const pageNum = ref(1);
  //总页数
  const totalNum = ref("");
  //反馈类型
  const opinionSecond = ref({});
  // 获取分页数据
  function getFeedList() {
    queryFeedback({
      pageSize: pageSize.value,
      pageNumber: pageNum.value,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
      opinionSecond.value = res.data.opinionSecond;
    });
  }
  //详情
  function detail(id) {
    router.push({
      name: "feedbackDetail",
      query: {
        id: id,
      },
    });
  }
  //处理
  function handle(row) {
    router.push({
      name: "feedbackHandle",
      query: {
        id: row.id,
        currentHandlerId: row.currentHandlerId,
      },
    });
  }
  //搜索表单
  const searchForm = reactive({});
  //状态下拉数据
  const statusOptions = ref([
    {
      id: 0,
      name: "审核中",
    },
    {
      id: 1,
      name: "处理中",
    },
    {
      id: 2,
      name: "已完结",
    },
  ]);
  const searchRef = ref(null);
  //用户id
  const userId = ref("");
  //搜索
  function search() {
    queryFeedback({
      pageSize: pageSize.value,
      pageNumber: pageNum.value,
      keyword: searchForm.name,
      startTime: searchForm.startTime,
      endTime: searchForm.endTime,
      status: searchForm.status,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
    });
  }
  //重置
  function reset() {
    searchRef.value.resetFields();
    getFeedList();
  }
  const route = useRoute();
  //监听页面变化，解决返回上一页，数据不刷新问题
  watch(
    () => route.path,
    () => {
      if (route.path === "/canteen/feedback") {
        getFeedList();
      }
    }
  );
  onMounted(() => {
    getFeedList();
  });

  return {
    userId,
    detail,
    tableData,
    handle,
    pageNum,
    pageSize,
    totalNum,
    getFeedList,
    searchForm,
    statusOptions,
    search,
    searchRef,
    reset,
    opinionSecond,
  };
}
