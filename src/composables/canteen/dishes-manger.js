import { onMounted, reactive, ref, watch } from "vue";
import router from "@/router";
import {
  delFood,
  delFoods,
  downFood,
  downFoods,
  getCanteenList,
  queryDishesList,
  upperFood,
  foodAudit,
} from "../../api/canteen/dishes-managerApi";
import { useRoute } from "vue-router";
import { ElMessage, ElMessageBox } from "element-plus";

export default function dishesManger() {
  const tableData = ref([]);
  const option = ref([]);
  const filterStatus = ref([
    { text: "已上架", value: "已上架" },
    { text: "未上架", value: "未上架" },
  ]);
  const filterCanteen = ref([]);
  const filterType = ref([
    { text: "荤菜", value: "荤菜" },
    { text: "素菜", value: "素菜" },
  ]);
  const route = useRoute();
  const multipleTable = ref(null);
  //新增
  function edit(row) {
    router.push({
      name: "DishesEdit",
      query: {
        id: row.id ? row.id : "",
      },
    });
  }
  const totalNum = ref(Number);
  const pageSize = ref(50);
  const pageNum = ref(1);
  //菜品类型
  const foodType = ref({});
  //获取菜品分页查询数据
  function getDishesData() {
    queryDishesList({
      pageNum: pageNum.value,
      pageSize: pageSize.value,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
      foodType.value = res.data.content.foodType;
    });
  }
  //多选数组ID集合
  const ids = ref([]);
  const multipleDta = ref([]);
  //批量上架
  function upper() {
    multipleDta.value.map((item) => {
      ids.value.push(item.id);
    });
    upperFood(ids.value).then(() => {
      ElMessage({
        message: "上架成功",
        type: "success",
      });
      getDishesData();
      ids.value = [];
    });
  }
  //批量下架
  function down() {
    if (multipleDta.value.length !== 0) {
      const menuIds = multipleDta.value.filter((item) => item.status === 4);
      if (menuIds.length === 0) {
        ElMessage.warning("只能下架在售中的数据");
      } else {
        menuIds.map((item) => {
          ids.value.push(item.id);
        });
        downFood(ids.value).then(() => {
          ElMessage({
            message: "下架成功",
            type: "success",
          });
          getDishesData();
          ids.value = [];
        });
      }
    } else {
      ElMessage.warning("请选择数据后再试");
    }
  }
  //多选方法
  function handleSelectionChange(val) {
    multipleDta.value = val;
  }
  const optionValue = ref("");
  //删除
  function del(id) {
    ElMessageBox.confirm("您确定要删除这条数据么?", "提示", {
      confirmButtonText: "确定",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        delFood(id).then(() => {
          ElMessage({
            message: "删除成功",
            type: "success",
          });
          getDishesData();
        });
      })
      .catch(() => {
        ElMessage({
          message: "取消操作",
          type: "info",
        });
      });
  }
  //上架下架方法
  function changeUpper(row) {
    //  下架
    downFoods(row.id).then(() => {
      ElMessage({
        message: "下架成功",
        type: "success",
      });
      getDishesData();
    });
  }
  //获取食堂列表数据
  const canteenData = ref([]);
  function getCanteenData() {
    getCanteenList().then((res) => {
      canteenData.value = res.data;
      canteenData.value.map((item) => {
        filterCanteen.value.push({ text: item.name, value: item.id });
      });
    });
  }
  //搜索数据表单
  const searchForm = reactive({});
  const searchRef = ref(null);
  //搜索功能
  function search() {
    queryDishesList({
      pageNum: pageNum.value,
      pageSize: pageSize.value,
      canteenId: searchForm.canteenId,
      name: searchForm.name,
      number: searchForm.number,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
    });
  }
  //重置表单
  function reset() {
    searchRef.value.resetFields();
    getDishesData();
  }
  watch(
    () => route.path,
    () => {
      if (route.path === "/canteen/dishes-manger") {
        getDishesData();
      }
    }
  );
  onMounted(() => {
    getDishesData();
    getCanteenData();
  });
  //全选删除
  function mutipleDel() {
    if (multipleDta.value.length === 0) {
      ElMessage.warning("请先选择数据再操作");
    } else {
      ElMessageBox.confirm("您确定要删除这条数据么?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          const ids = [];
          multipleDta.value.map((item) => {
            if (item.status === 1 || item.status === 3) {
              ids.push(item.id);
            }
          });
          delFoods(ids).then(() => {
            ElMessage.success("批量删除成功");
            getDishesData();
          });
        })
        .catch(() => {
          ElMessage.info("取消操作");
        });
    }
  }
  function selectCanteen(column, val) {
    console.log(column);
    console.log(val);
    // queryDishesList({
    //   pageNum: pageNum.value,
    //   pageSize: pageSize.value,
    //   canteenId: val[0],
    // }).then((res) => {
    //   tableData.value = res.data.content;
    //   totalNum.value = res.data.totalElements;
    // });
  }
  // 跳转到详情页面
  function toDetail(id) {
    router.push({
      name: "DishesDetail",
      query: {
        id: id,
      },
    });
  }
  // 批量提交
  function mutipleSubmit() {
    if (multipleDta.value.length === 0) {
      ElMessage.warning("请先选择数据后再操作");
    } else {
      const dishesIds = multipleDta.value.filter((item) => item.status === 1);
      if (dishesIds.length === 0) {
        ElMessage.warning("只能提交待待提交状态数据");
      } else {
        const ids = [];
        dishesIds.map((item) => {
          ids.push(item.id);
        });
        foodAudit(ids).then(() => {
          ElMessage.success("提交审核成功");
          getDishesData();
        });
      }
    }
  }
  // 菜品单个提交
  function foodSignSubmit(id) {
    let ids = [];
    ids.push(id);
    foodAudit(ids).then((res) => {
      if (res.code === 200) {
        ElMessage.success("提交审核成功");
        getDishesData();
        ids = [];
      } else {
        ElMessage.error(res.msg);
        ids = [];
      }
    });
  }
  return {
    selectCanteen,
    option,
    filterStatus,
    filterType,
    filterCanteen,
    tableData,
    optionValue,
    edit,
    pageNum,
    pageSize,
    totalNum,
    getDishesData,
    multipleTable,
    upper,
    handleSelectionChange,
    down,
    del,
    changeUpper,
    searchForm,
    searchRef,
    search,
    canteenData,
    reset,
    mutipleDel,
    toDetail,
    mutipleSubmit,
    foodType,
    foodSignSubmit,
  };
}
