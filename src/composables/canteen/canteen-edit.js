import { onMounted, reactive, ref } from "vue";
import { useRoute, useRouter } from "vue-router";
import {
  addCanteen,
  editCanteen,
  queryCanteen,
} from "../../api/canteen/canteen-settingApi";
import { ElMessage } from "element-plus";
import { checkNum } from "../../utils";

export default function canteenEdit() {
  const canteenForm = reactive({});
  const route = useRoute();
  const router = useRouter();
  const fileList = ref([]);

  const formRef = ref(null);
  //用户ID
  const userId = ref("");
  //放回上一页方法
  function back() {
    router.back(-1);
  }
  //提交表单操作
  function submit() {
    formRef.value.validate((valid) => {
      if (valid) {
        if (!userId.value) {
          addCanteen({
            name: canteenForm.name,
            address: canteenForm.address,
            memo: canteenForm.memo,
            image: canteenForm.image,
            sort: canteenForm.sort,
            openStartTime: canteenForm.openStartTime,
            openEndTime: canteenForm.openEndTime,
          }).then(() => {
            ElMessage({
              message: "添加成功",
              type: "success",
            });
            //  清空表单数据
            formRef.value.resetFields();
            back();
          });
        } else {
          editCanteen(userId.value, {
            id: userId.value,
            address: canteenForm.address,
            name: canteenForm.name,
            memo: canteenForm.memo,
            openEndTime: canteenForm.openEndTime,
            openStartTime: canteenForm.openStartTime,
            image: canteenForm.image,
          })
            .then(() => {
              ElMessage({
                message: "更新成功",
                type: "success",
              });
              //  清空表单数据
              formRef.value.resetFields();
              back();
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }
    });
  }
  //取消操作
  function cancel() {
    back();
  }
  onMounted(() => {
    userId.value = route.query.id;
    if (userId.value) {
      queryCanteen(userId.value).then((res) => {
        const canteenInfo = res.data;
        canteenForm.name = canteenInfo.name;
        canteenForm.address = canteenInfo.address;
        canteenForm.image = canteenInfo.image;
        canteenForm.memo = canteenInfo.memo;
        canteenForm.sort = canteenInfo.sort;
        canteenForm.openStartTime = canteenInfo.openStartTime;
        canteenForm.openEndTime = canteenInfo.openEndTime;
        fileList.value.push({ url: canteenForm.image });
      });
    } else {
      formRef.value.resetFields();
    }
  });
  const formRule = ref({
    name: [{ required: true, message: "请输入名称", trigger: "blur" }],
    address: [{ required: true, message: "请输入地址", trigger: "blur" }],
    sort: [{ required: true, validator: checkNum, trigger: "blur" }],
  });
  function handleOnExceed() {
    ElMessage.warning("只能上传一张图片，请删除图片后重试");
  }
  return {
    handleOnExceed,
    formRule,
    back,
    canteenForm,
    cancel,
    formRef,
    submit,
    fileList,
  };
}
