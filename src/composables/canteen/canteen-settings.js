import { onMounted, reactive, ref, watch } from "vue";
import router from "@/router";
import { ElMessage, ElMessageBox } from "element-plus";
import {
  delCanteen,
  delMultipleCanteen,
  getCanteenPage,
} from "../../api/canteen/canteen-settingApi";
import { useRoute } from "vue-router";

export default function canteenSettings() {
  //表格数据
  const tableData = ref([]);
  const searchRef = ref(null);
  //编辑操作
  function edit(row) {
    router.push({
      name: "CanteenEdit",
      query: {
        id: row.id ? row.id : "",
      },
    });
  }
  const route = useRoute();
  //用户ID集合
  const ids = ref([]);
  //搜索表单数据
  const searchForm = reactive({});
  //多选删除操作
  function delMultiple() {
    if (multipleTable.value.length !== undefined) {
      ElMessageBox.confirm("您确定要删除这条数据么?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          multipleTable.value.map((item) => {
            ids.value.push(item.id);
          });
          delMultipleCanteen(ids.value).then(() => {
            ElMessage({
              message: "批量删除成功",
              type: "success",
            });
            getCanteenData();
          });
        })
        .catch(() => {
          ElMessage.info("取消操作");
        });
    } else {
      ElMessage.warning("请先选择需要删除的数据");
    }
  }
  //多选操作
  function handleSelectionChange(val) {
    multipleTable.value = val;
  }
  //搜索功能
  function search() {
    getCanteenPage({
      pageSize: pageSize.value,
      pageNum: pageNum.value,
      name: searchForm.name,
    }).then((res) => {
      tableData.value = res.data.content;
      ElMessage({
        message: "查询成功",
        type: "success",
      });
    });
  }
  //重置
  function reset() {
    searchRef.value.resetFields();
    getCanteenData();
  }
  const totalNum = ref(Number);
  const pageSize = ref(50);
  const pageNum = ref(1);
  //多选数组
  const multipleTable = ref([]);
  //获取所有食堂信息
  function getCanteenData() {
    getCanteenPage({
      pageSize: pageSize.value,
      pageNum: pageNum.value,
    }).then((res) => {
      tableData.value = res.data.content;
      totalNum.value = res.data.totalElements;
    });
  }
  //删除操作
  function del(id) {
    ElMessageBox.confirm("您确定要删除这条数据么?", "提示", {
      confirmButtonText: "确定",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        delCanteen(id).then(() => {
          ElMessage({
            type: "success",
            message: "操作成功",
          });
          getCanteenData();
        });
      })
      .catch(() => {
        ElMessage({
          type: "info",
          message: "取消操作",
        });
      });
  }
  //监听页面变化，解决返回上一页，数据不刷新问题
  watch(
    () => route.path,
    () => {
      if (route.path === "/canteen/canteen-settings") {
        getCanteenData();
      }
    }
  );
  onMounted(() => {
    getCanteenData();
  });
  return {
    getCanteenData,
    tableData,
    del,
    totalNum,
    pageSize,
    pageNum,
    delMultiple,
    reset,
    search,
    edit,
    searchRef,
    multipleTable,
    handleSelectionChange,
    searchForm,
  };
}
