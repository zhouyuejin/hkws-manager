// index.js
const Mock = require("mockjs");
const mockFiles = require.context("./modules", false, /\.js$/);
let mocks = [];

mockFiles.keys().forEach((key) => {
  mocks.push(...mockFiles(key).default);
});

mocks.forEach((item) => {
  // 处理url后面的参数也被拦截
  Mock.mock(RegExp(item.url + ".*"), item.type, item.response);
});
