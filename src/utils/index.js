export function param2Obj(url) {
  const search = url.split("?")[1];
  if (!search) {
    return {};
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"') +
      '"}'
  );
}
//验证手机号码
export function checkPhone(rule, value, callback) {
  if (value) {
    const reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
    if (!reg.test(value)) {
      return callback(new Error("请输入正确的手机号"));
    } else {
      callback();
    }
  }
  callback();
}
//验证输入框输入汉字
export function checkData(rule, value, callback) {
  if (value) {
    if (/[\u4E00-\u9FA5]/g.test(value)) {
      return callback(new Error("不能包含中文!"));
    } else {
      callback();
    }
  }
  callback();
}
export function checkNum(rule, value, callback) {
  if (value) {
    const reg = /^[0-9]*[1-9][0-9]*$/;
    if (!reg.test(value)) {
      return callback(new Error("只能输入数字"));
    } else {
      callback();
    }
  }
  callback();
}
//数字转换为字符串
export function numToStr(num) {
  num = num.toString();
  return num;
}
