/* 产引入jsencrypt实现数据RSA加密 */
import JSEncrypt from "jsencrypt"; // 处理长文本数据时报错 jsencrypt.js Message too long for RSA
/* 产引入encryptlong实现数据RSA加密 */
import Encrypt from "encryptlong"; // encryptlong是基于jsencrypt扩展的长文本分段加解密功能
const publicKey =
  "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCIQhxHCrZHUgVU3bQYMwcnPO6bz1C1cvCi6PaXu5qPDQeN4J/VcBTzG8fs0H8dNxcHdS8FTs6YNm1tZ23AFOZxjpt+KKDvJXrv+xxOQ5PiFfYBswdzIIsvUAhAQ5diINHla9uGgv/y86e7YZSgpu6rzyxPVM6aEG4S/v2Cc6Q+8wIDAQAB";
export default {
  /* JSEncrypt加密 */
  rsaPublicData(data) {
    var jsencrypt = new JSEncrypt();
    jsencrypt.setPublicKey(publicKey);
    // 如果是对象/数组的话，需要先JSON.stringify转换成字符串
    var result = jsencrypt.encrypt(data);
    return result;
  },
  /* 加密 */
  encrypt(data) {
    const PUBLIC_KEY = publicKey;
    var encryptor = new Encrypt();
    encryptor.setPublicKey(PUBLIC_KEY);
    // 如果是对象/数组的话，需要先JSON.stringify转换成字符串
    const result = encryptor.encryptLong(data);
    return result;
  },
};
