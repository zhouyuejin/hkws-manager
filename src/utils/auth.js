import Cookies from "js-cookie";
const TokenKey = "token";
//获取token
export function getToken() {
  return Cookies.getItem(TokenKey);
}
//设置token
export function setToken(token) {
  return Cookies.setItem(TokenKey, token);
}
//移除token
export function removeToken() {
  return Cookies.remove(TokenKey);
}
