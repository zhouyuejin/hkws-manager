/**
 * 判断用户是否拥有操作权限
 * 根据传入的权限标识，查看是否存在用户权限标识集合
 * @param perms
 */
export function hasPermission(perms) {
  let hasPermission = false;
  let permission = JSON.parse(sessionStorage.getItem("authorities"));
  for (let i = 0, len = permission.length; i < len; i++) {
    if (permission[i] === perms) {
      hasPermission = true;
      break;
    }
  }
  return hasPermission;
}
