const user = {
  state: {
    name: "",
    avatar: "",
    permissions: [],
    userInfo: {},
  },
  mutations: {
    SET_PERMISSION(state, permission) {
      //permission 为传入的权限标识集合
      // 传入的权限集合 赋值给状态中的 permission
      permission.map((item) => {
        state.permissions.push(item);
      });
      state.permissions = permission;
    },
    setUserInfo(state, userinfo) {
      state.userInfo = userinfo;
    },
  },
  actions: {
    SET_PERMISSION(context, permission) {
      // 提交到 mutation 中的 SET_PERMISSION 函数
      context.commit("SET_PERMISSION", permission);
    },
  },
};
export default user;
