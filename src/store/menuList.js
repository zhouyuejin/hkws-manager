const menuList = {
  state: {
    //    顶部菜单
    topMenuList: [
      {
        icon: require("@/assets/icons/top-menu-icon/canteen.png"),
        name: "食堂管理",
        path: "/canteen",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/sushe.png"),
        name: "宿舍管理",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/work.png"),
        name: "工作台",
        path: "/work",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/caiwu.png"),
        name: "财务管理",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/qiantai.png"),
        name: "前台业务",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/renshi.png"),
        name: "人事管理",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/baobiao.png"),
        name: "报表中心",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/system.png"),
        name: "系统设置",
        path: "/system",
        isAuth: false,
      },
      {
        icon: require("@/assets/icons/top-menu-icon/message.png"),
        name: "消息中心",
        path: "/message",
        isAuth: false,
      },
    ],
    //    左侧菜单：
    leftMenuList: [
      {
        icon: "el-icon-setting",
        name: "基础设置",
        path: "/system/basic-settings",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-location-information",
        name: "区域设置",
        path: "/system/regional-settings",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-user",
        name: "部门设置",
        path: "/system/department-setting",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-edit-outline",
        name: "操作日志",
        path: "/system/operation-log",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-user",
        name: "用户管理",
        path: "/system/user-management",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-s-custom",
        name: "角色管理",
        path: "/system/role-management",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-menu",
        name: "菜单管理",
        path: "/system/menu-management",
        module: "/system",
        isAuth: false,
      },
      {
        icon: "el-icon-s-operation",
        name: "食堂配置",
        path: "/canteen/canteen-settings",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-fork-spoon",
        name: "菜谱管理",
        path: "/canteen/menu-manger",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-reading",
        name: "菜品管理",
        path: "/canteen/dishes-manger",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-edit-outline",
        name: "意见反馈",
        path: "/canteen/feedback",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-edit",
        name: "评论管理",
        path: "/canteen/comment-manger",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-document-checked",
        name: "食堂审核",
        path: "/canteen/audit",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-school",
        name: "包房管理",
        path: "/canteen/roomManger",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-notebook-2",
        name: "套餐管理",
        path: "/canteen/meal",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-coffee-cup",
        name: "餐标管理",
        path: "/canteen/mealLabel",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-office-building",
        name: "包房预约",
        path: "/canteen/order",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-edit-outline",
        name: "包房审核",
        path: "/canteen/roomAudit",
        module: "/canteen",
        isAuth: false,
      },
      {
        icon: "el-icon-document",
        name: "通知公告",
        path: "/message/notice",
        module: "/message",
        isAuth: false,
      },
      {
        icon: "el-icon-s-platform",
        name: "曝光台",
        path: "/message/expose",
        module: "/message",
        isAuth: false,
      },
      {
        icon: "el-icon-user",
        name: "食堂审核人",
        path: "/canteen/auditUser",
        module: "/canteen",
        isAuth: false,
      },
    ],
    //  是否加载过路由
    menuRouteLoaded: false,
  },
  mutations: {
    menuRouteLoaded(state, menuRouteLoaded) {
      // 改变菜单和路由的加载状态
      state.menuRouteLoaded = menuRouteLoaded;
    },
    //  设置菜单方法
    setMenu(state) {
      //  获取权限数组
      const authorities = sessionStorage.getItem("authorities");
      if (authorities) {
        state.leftMenuList.forEach((item) => {
          const index = authorities.indexOf(item.path);
          if (index > -1) {
            item.isAuth = true;
          }
        });
        state.topMenuList.forEach((item) => {
          const index = authorities.indexOf(item.path);
          if (index > -1) {
            item.isAuth = true;
          }
        });
      } else {
        return;
      }
    },
  },
  getters: {
    topMenu: (state) => {
      return state.topMenuList;
    },
    leftMenu: (state) => {
      return state.leftMenuList;
    },
  },
  actions: {},
};
export default menuList;
