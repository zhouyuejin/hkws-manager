import { createStore } from "vuex";
import api from "@/api";
import user from "./user";
import menuList from "./menuList";
export default createStore({
  namespaced: true,
  state: {
    count: 0,
    tableData: [],
    totalNum: 0,
    // 设置属性，用于保存后台接口返回的权限数据
    permission: [],
  },
  mutations: {
    addCount(state) {
      state.count++;
    },
    queryData(state, data) {
      state.tableData = data;
    },
    totalNum(state, num) {
      state.totalNum = num;
    },
    SET_PERMISSION(state, permission) {
      //permission 为传入的权限标识集合
      state.permission = permission;
    },
  },
  actions: {
    getData({ commit }, params) {
      api.user.getList(params).then((res) => {
        commit("queryData", res.data.data);
        commit("totalNum", res.data.totalNum);
      });
    },
  },
  modules: {
    menuList,
    user,
  },
});
