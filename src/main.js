import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import installElementPlus from "./plugins/element";
import TsButton from "./components/TsButton";

//引入通用样式文件
import("@/assets/style/common.scss");
//引入echarts
import echarts from "echarts";
//根据权限重新修改isAuth,为了防止强制刷新状态丢失
store.commit("setMenu");
// 修改原生滚动条样式
import("@/assets/style/scrollbar.css");
const app = createApp(App).use(store, echarts).use(router);
// 挂载elmentPlus-安装请用vue add element-plus
installElementPlus(app);
app.mount("#app");
//注册全局组件
app.component("TsButton", TsButton);
