"use strict";
module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: 9928,
    open: true,
    proxy: {
      //配置跨域
      "/api": {
        target:
          process.env.NODE_ENV === "development"
            ? "http://192.168.0.15:8080"
            : "http://47.105.128.122:8081", //本地服务器
        ws: true,
        changOrigin: true, //允许跨域
        pathRewrite: {
          "^/api": "", //请求时用api
        },
      },
      "/upload": {
        target: "http://47.105.128.122:8081",
        ws: true,
        changOrigin: true, //允许跨域
      },
    },
  },
};
